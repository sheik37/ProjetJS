$(function() {

    $("#buttonL1").click(refreshMatchList);
    $("#buttonLDC").click(refreshMatchList);
    $("#buttonPL").click(refreshMatchList);
    $("#statistique").click(refreshStatList);

    var championnat=null;

    function refreshMatchList(champ){
        championnat=champ.currentTarget.value
        $("#currentmatch").empty();
        requete="http://localhost:3000/matchs";
        fetch(requete)
        .then(response=>{
            if(response.ok)return response.json();
            else throw new Error('Problème ajax: '+response.status);
        })
        .then(remplirMatch)
        .catch(onerror)
        }

    function remplirMatch(matchs){
        $("#tools").empty();
        $("#tools")
            .append($('<h2>Editeur de matchs</h2>'))
            .append($('<img id="add" src="img/new.png" alt="Nouvelle chose a faire"/>'))
            .append($('<img id="del" src="img/delete.png" alt="Enlever cette tache"/>'));
        $("#tools #add").on("click", formMatch);
        $('#tools #del').on('click', delMatch);
        $('#matchs').empty();
        $('#matchs').append($('<h2>Liste des matchs</h2>'));
        $('#matchs').append($('<ul>'))
        for(var i=0;i<matchs.length;i++){
            //console.log(matchs[i].championnat);
            if(matchs[i].championnat==championnat){
                $('#matchs ul')
                .append($('<li>')
                .append($('<a>')
                .text(matchs[i].title)).on("click",matchs[i],details));
            }
        }
    }
    function onerror(err){
        $("#matchs").html("<b>Impossible de récupérer la liste des matchs !</b>"+err)
    }

    function details(event){
        $("#currentmatch").empty();
        formMatch();
        fillFormMatch(event.data);
        }

    // Objet Match en JS
    function Match(Domicile,Exterieur,date,heure,title,championnat,nul,VicDom,VicExt, uri){
        this.domicile = Domicile;
        this.exterieur = Exterieur;
        this.date=date;
        this.heure=heure;
        this.title=title;
        this.championnat=championnat;
        this.nul=nul;
        this.vicDom=VicDom;
        this.vicExt=VicExt;
        this.uri = uri;
        // console.log(this.uri);
    }

    function formMatch(isnew){
      $("#currentmatch").empty();
      $("#currentmatch")
          .append($('<section id="label"></section>'))
          $("#label")
            .append($('<p>Match : </p>'))
            .append($('<p>Championnat : </p>'))
            .append($('<p>Domicile : </p>'))
            .append($('<p>Exterieur : </p>'))
            .append($('<p>Victoire : </p>'))
            .append($('<p>Nul : </p>'))
            .append($('<p>Victoire exterieure : </p>'))
            .append($('<p>Date : </p>'))
            .append($('<p>Heure : </p>'))
      $("#currentmatch")
          .append($('<section id="input"></section>'))
          $("#input")
            .append($('<span><input type="text" id="titre"></br></span>'))
            .append($('<span><select id="championnat"><option value="Ligue des champions">Ligue des champions</option><option value="Premiere league">Premiere league</option><option value="Ligue 1">Ligue 1</option></select></br></span>'))

            .append($('<span><input type="text" id="domicile"></br></span>'))
            .append($('<span><input type="text" id="exterieur"></br></span>'))
            .append($('<span><input type="checkbox" id="vicdom"></br></span>'))
            .append($('<span><input type="checkbox" id="nul"></br></span>'))
            .append($('<span><input type="checkbox" id="vicext"></br></span>'))
            .append($('<span><input type="date" id="date"></br></span>'))
            .append($('<span><input type="time" id="heure"></br></span>'))
            .append($('<span><input type="hidden" id="turi"></span>'))
             .append(isnew?$('<span><input class="button" type="button" value="Save Match"><br></span>').on("click", saveNewMatch)
                       :$('<span><input class="button" type="button" value="Modify Match"><br></span>').on("click", saveModifiedMatch)
              );
        }

    function fillFormMatch(t){
        $("#currentmatch #titre").val(t.title);
        $("#currentmatch #championnat").val(t.championnat);
        $("#currentmatch #domicile").val(t.domicile);
        $("#currentmatch #exterieur").val(t.exterieur);
        $("#currentmatch #date").val(t.date);
        $("#currentmatch #heure").val(t.heure);
         t.uri=(t.uri == undefined)?"http://localhost:3000/matchs/"+t.id:t.uri;
         $("#currentmatch #turi").val(t.uri);
        t.vicExt?$("#currentmatch #vicext").prop('checked', true):
        $("#currentmatch #vicext").prop('checked', false);
        t.vicDom?$("#currentmatch #vicdom").prop('checked', true):
        $("#currentmatch #vicdom").prop('checked', false);
        t.nul?$("#currentmatch #nul").prop('checked', true):
        $("#currentmatch #nul").prop('checked', false);
    }

    function saveNewMatch(){
        var match = new Match(
            $("#currentmatch #domicile").val(),
            $("#currentmatch #exterieur").val(),
            $("#currentmatch #date").val(),
            $("#currentmatch #heure").val(),
            $("#currentmatch #domicile").val()+"-"+$("#currentmatch #exterieur").val(),
            $("#currentmatch #championnat").val(),
            $("#currentmatch #nul").is(':checked'),
            $("#currentmatch #vicdom").is(':checked'),
            $("#currentmatch #vicext").is(':checked')
            );
        // console.log(JSON.stringify(match));
        fetch("http://localhost:3000/matchs/",{
            headers:{
                'Accept':'application/json',
                'content-Type':'application/json'
            },
            method:"POST",
            body:JSON.stringify(match)
        })
        .then(res=>{
            console.log('Save Sucess');
            $("#result").text(res['contenu'])
        })
        .catch(res=>{console.log(res)})
        refreshMatchList();
    }

    function saveModifiedMatch(){
        var match = new Match(
            $("#currentmatch #domicile").val(),
            $("#currentmatch #exterieur").val(),
            $("#currentmatch #date").val(),
            $("#currentmatch #heure").val(),
            $("#currentmatch #titre").val(),
            $("#currentmatch #championnat").val(),
            $("#currentmatch #nul").is(':checked'),
            $("#currentmatch #vicdom").is(':checked'),
            $("#currentmatch #vicext").is(':checked'),
            $("#currentmatch #turi").val()
            );
            // console.log(JSON.stringify(match));
            $.ajax({
                url:match.uri,
                type:'PUT',
                contentType:'application/json',
                data:JSON.stringify(match),
                dataType:"json",
                success:function(msg){
                    alert('Save Success');
                },
                error: function (err){
                    alert('Save Error');
                }
            });
        refreshMatchList();
    }

    function delMatch(){
        var match = new Match(
            $("#currentmatch #domicile").val(),
            $("#currentmatch #exterieur").val(),
            $("#currentmatch #date").val(),
            $("#currentmatch #heure").val(),
            $("#currentmatch #titre").val(),
            $("#currentmatch #championnat").val(),
            $("#currentmatch #nul").is(':checked'),
            $("#currentmatch #vicdom").is(':checked'),
            $("#currentmatch #vicext").is(':checked'),
            $("#currentmatch #turi").val()
            );
        $.ajax({
            url: match.uri,
            type: 'DELETE',
            contentType: 'application/json',
            data: JSON.stringify(match),
            dataType: 'json',
            success: function (msg) {
                alert('Save Success');
            },
            error: function (err){
                alert('Save Error');
            }
            });
        refreshMatchList();
    }

    function refreshStatList(){
        $("#currentmatch").empty();
        requete="http://localhost:3000/ligue";
        fetch(requete)
        .then(response=>{
            if(response.ok)return response.json();
            else throw new Error('Problème ajax: '+response.status);
        })
        .then(remplirStat)
        .catch(onerrorstat)
        }


    function remplirStat(ligue){


        $("#tools").empty();
        $("#tools")
            .append($('<h2>Classement des ligues</h2>'))
        $('#matchs').empty();
        $('#matchs').append($('<h2>Liste des ligues</h2>'));
        $('#matchs').append($('<ul>'))
        for(var i=0;i<ligue.length;i++){
            $('#matchs ul')
            .append($('<li>')
            .append($('<a>')
            .text(ligue[i].nom)).on("click",ligue[i],detailstat));

        }
    }

    function onerrorstat(err){
        $("#matchs").html("<b>Impossible de récupérer la liste des ligues !</b>"+err)
    }

    var ligueSelect= null;

    function detailstat(event){
        $("#tools").empty();
        $("#currentmatch").empty();
        ligueSelect = event.data.code;
        formStat();
        }

    function formStat(){


        $("#tools").empty();
        $("#tools")
            .append($('<h2>Classement des ligues</h2>'))
        $("#currentmatch").empty();

        //console.log(ligueSelect.code);

        var js, fjs = document.getElementsByTagName("script")[0];
        console.log(ligueSelect);
        js = document.createElement("script");
        js.id = "statsfc-table-js";
        js.src = "https://statsfc-4f51.kxcdn.com/widget/table-2.0.js";
        fjs.parentNode.insertBefore(js, fjs);
        console.log("OK");
        $("#currentmatch").append('<div class="statsfc-table" data-key="Gjl99Z7NL6xu684bWhs5bqrDtDChzka50syPpDHt" data-competition="'+ligueSelect+'" data-table-type="full" data-show-badges="false" data-show-form="false" data-omit-errors="false" data-use-default-css="false" data-lang="en"></div>')

      }

});
